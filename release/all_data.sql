-- This file must be manually modified as order matters when creating data records
-- Data files are re-runnable data scripts
-- ex: @../data/data_my_table.sql

@../data/data_px_roles.sql
@../data/data_sa_ref_code_types.sql
@../data/data_sa_ref_codes.sql